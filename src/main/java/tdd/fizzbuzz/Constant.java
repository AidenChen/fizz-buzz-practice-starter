package tdd.fizzbuzz;

@SuppressWarnings("all")
public interface Constant {

    String FIZZ = "Fizz";

    String BUZZ = "Buzz";

    String FIZZBUZZ = "FizzBuzz";

    String WHIZZ = "Whizz";

    String FIZZWHIZZ = "FizzWhizz";

    String BUZZWHIZZ = "BuzzWhizz";
}
