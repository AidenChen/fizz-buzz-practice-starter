package tdd.fizzbuzz;

public class FizzBuzz {


    // 3_5,5_7

    public String sayWord(int number) {
        if (number % 3 == 0 && number % 5 == 0) {
            return Constant.FIZZBUZZ;
        }
        if (number % 3 == 0 && number % 7 == 0) {
            return Constant.FIZZWHIZZ;
        }
        if (number % 5 == 0 && number % 7 == 0) {
            return Constant.BUZZWHIZZ;
        }
        if (number % 3 == 0) {
            return Constant.FIZZ;
        }
        if (number % 5 == 0) {
            return Constant.BUZZ;
        }
        if (number % 7 == 0) {
            return Constant.WHIZZ;
        }
        return null;
    }

}
