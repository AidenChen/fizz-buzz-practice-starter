package tdd.fizzbuzz;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SuppressWarnings("all")
public class FizzBuzzTest {




    @Test
    void should_say_fizz_when_FizzBuzz_given_is_three() {

        FizzBuzz fizzBuzz = new FizzBuzz();
        //given
        int number = 3;
        //when
        String word = fizzBuzz.sayWord(number);
        //then
        assertEquals(Constant.FIZZ, word);
    }

    @Test
    void should_say_fizz_when_FizzBuzz_given_is_five() {
        FizzBuzz fizzBuzz = new FizzBuzz();
        //given
        int number = 5;
        //when
        String word = fizzBuzz.sayWord(number);
        //then
        assertEquals(Constant.BUZZ, word);
    }

    @Test
    void should_say_fizz_buzz_when_FizzBuzz_given_is_mulitiple_of_five_and_three() {
        FizzBuzz fizzBuzz = new FizzBuzz();
        //given
        int number = 15;
        //when
        String word = fizzBuzz.sayWord(number);
        //then
        assertEquals(Constant.FIZZBUZZ, word);
    }

    @Test
    void should_say_whizz_when_FizzBuzz_given_is_mulitiple_of_seven() {
        FizzBuzz fizzBuzz = new FizzBuzz();
        //given
        int number = 7;
        //when
        String word = fizzBuzz.sayWord(number);
        //then
        assertEquals(Constant.WHIZZ, word);
    }

    @Test
    void should_say_word_when_FizzBuzz_given_is_mulitiple_of_lot() {
        FizzBuzz fizzBuzz = new FizzBuzz();
        //given
        int number1 = 21;
        int number2 = 35;
        //when
        String word1 = fizzBuzz.sayWord(number1);
        String word2 = fizzBuzz.sayWord(number2);
        //then
        assertEquals(Constant.FIZZWHIZZ, word1);
        assertEquals(Constant.BUZZWHIZZ, word2);

    }

}
